import 'phaser';
import PongScene from './scenes/PongScene';

var config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
      default: 'arcade',
      arcade: {
        gravity: { 
          y: 0, 
          x: 0
        }
      }
  },
  scene: [
    PongScene,
  ]
};

var game = new Phaser.Game(config);
