export class ScoreKeeper {
  constructor(gameObject, ball, scoreZones = [], scores = [], scoreCard = {}) {
    this.gameObject = gameObject;
    this.ball = ball;
    this.scoreZones = scoreZones;
    this.scores = scores;
    this.scoreCard = scoreCard;
    
    
    
    
    
    
  }

  triggerScore(flag) {
    const point = this.scoreCard[flag];
    const gamePoint = this.scores[point].incrementScore(1);
    if (gamePoint === true) {
      console.log(`${flag} wins`);
    }
    console.log(this.scores[point].currentScore)
  }
}
