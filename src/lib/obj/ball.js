const RandomCrap = require('randomcrap');

export class ball {
  constructor(gameObject, x = 0, y = 0) {
    this.self = gameObject.add.graphics();
    this.gameObject = gameObject;
    this.x = x;
    this.y = y;
    this.size = 1;
    this.scale = 1;
    this.random = new RandomCrap();
    this.ballWidth = 12;

    this.self.fillStyle(0xffffff, 1);
    this.self.fillRect(this.x, this.y, this.ballWidth, this.ballWidth);
    this.self.generateTexture('ball', this.ballWidth, this.ballWidth);
    this.self.setVisible(false);
  }

  setScoreZones(scoreZones) {
    this.scoreZones = scoreZones;
  }

  getScoreZones() {
    return this.scoreZones;
  }

  // direction is a x/y object.
  serve(direction, scoreZones = []) {
    const x = this.random.simpleRandomInt(395, 405);
    const y = this.random.simpleRandomInt(250, 350);
    
    this.ballP = this.gameObject.physics.add.image(x, y, 'ball');
    this.ballP
      .setVelocity(direction.x * this.random.randomFrom([-1, 1,- 1, 1, -1, 1,], 'direction', true), direction.y)
      .setBounce(1)
      .setCollideWorldBounds(true);

    if (scoreZones.length > 0) {
      this.setScoreZones(scoreZones);
    }

    for (let iterator = 0; iterator < this.scoreZones.length; iterator ++) {
      const _this = this;
      // Bodging this faulting overlap detection.
      // console.log(this.scoreZones[iterator]);
      // this.gameObject.physics.add.overlap(
      //   this.ballP, 
      //   this.scoreZones[iterator], 
      //   function() { 
      //     _this.reserve(_this.scoreZones[iterator].texture.key);
      //   }
      // );
    }
  }

  reserve(key) {
    console.log(key);

    this.ballP.destroy();
    this.ballP = undefined;
    this.serve(
      { 
        x: this.random.simpleRandomInt(200, 300), 
        y: this.random.simpleRandomInt(-170, 170)
      }
    );
  }
}
