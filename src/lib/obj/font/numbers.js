import { Math } from 'phaser';

export class numbers {
  constructor(gameObject, x = 0, y = 0) {
    this.self = gameObject.add.graphics();
    this.x = x;
    this.y = y;
    this.size = 10;
    this.scale = 1;
    this.rects = {
      zero: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 3,
            height: 1,
          },
          {
            x: 0,
            y: 0,
            width: 1,
            height: 7,
          },
          {
            x: 0,
            y: 7,
            width: 3,
            height: 1,
          },
          {
            x: 3,
            y: 0,
            width: 1,
            height: 8,
          },
        ]
      },
      one: { 
        rectangles: [
          {
            x: 3,
            y: 0,
            width: 1,
            height: 8,
          },
        ]
      },
      two: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 3,
            height: 1,
          },
          {
            x: 3,
            y: 0,
            width: 1,
            height: 4,
          },
          {
            x: 0,
            y: 7,
            width: 4,
            height: 1,
          },
          {
            x: 0,
            y: 3,
            width: 3,
            height: 1,
          },
          {
            x: 0,
            y: 4,
            width: 1,
            height: 3,
          },
        ]
      },
      three: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 3,
            height: 1,
          },
          {
            x: 0,
            y: 7,
            width: 3,
            height: 1,
          },
          {
            x: 0,
            y: 3,
            width: 3,
            height: 1,
          },
          {
            x: 3,
            y: 0,
            width: 1,
            height: 8,
          },
        ]
      },
      four: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 1,
            height: 4,
          },
          {
            x: 0,
            y: 3,
            width: 3,
            height: 1,
          },
          {
            x: 3,
            y: 0,
            width: 1,
            height: 8,
          },
        ]
      },
      five: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 4,
            height: 1,
          },
          {
            x: 3,
            y: 4,
            width: 1,
            height: 4,
          },
          {
            x: 0,
            y: 7,
            width: 4,
            height: 1,
          },
          {
            x: 0,
            y: 3,
            width: 4,
            height: 1,
          },
          {
            x: 0,
            y: 0,
            width: 1,
            height: 3,
          },
        ]
      },
      six: { 
        rectangles: [
          
          {
            x: 3,
            y: 4,
            width: 1,
            height: 4,
          },
          {
            x: 0,
            y: 7,
            width: 4,
            height: 1,
          },
          {
            x: 0,
            y: 3,
            width: 4,
            height: 1,
          },
          {
            x: 0,
            y: 0,
            width: 1,
            height: 7,
          },
        ]
      },
      seven: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 3,
            height: 1,
          },
          {
            x: 3,
            y: 0,
            width: 1,
            height: 8,
          },
        ]
      },
      eight: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 3,
            height: 1,
          },
          {
            x: 0,
            y: 0,
            width: 1,
            height: 7,
          },
          {
            x: 0,
            y: 7,
            width: 3,
            height: 1,
          },
          {
            x: 0,
            y: 3,
            width: 3,
            height: 1,
          },
          {
            x: 3,
            y: 0,
            width: 1,
            height: 8,
          },
        ]
      },
      nine: { 
        rectangles: [
          {
            x: 0,
            y: 0,
            width: 3,
            height: 1,
          },
          {
            x: 0,
            y: 0,
            width: 1,
            height: 4,
          },
          {
            x: 0,
            y: 3,
            width: 3,
            height: 1,
          },
          {
            x: 3,
            y: 0,
            width: 1,
            height: 8,
          },
        ]
      },
    };
    this.num = [
      'zero',
      'one',
      'two',
      'three',
      'four',
      'five',
      'six',
      'seven',
      'eight',
      'nine',
    ];
  }

  draw(num) {
    const rectangles = this.rects[this.num[num]].rectangles;

    this.self.fillStyle(0xffffff, 1);

    for (let i = 0; i < rectangles.length; i ++) {
      this.self.fillRect(
        this.x + rectangles[i].x * this.size,
        this.y + rectangles[i].y * this.size,
        rectangles[i].width * this.size,
        rectangles[i].height * this.size
      );
    };
  }

  setX(n) {
    this.x = n;
  }

  setY(n) {
    this.y = n;
  }

  setVisible() {
    this.self.setVisible(true);
  }

  setInVisible() {
    this.self.setVisible(false);
  }

  getScaledValue(n) {
    const i = Math.MinSub((n * this.scale * this.size), this.size,  0);
    return i;
  }
}