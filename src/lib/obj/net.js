export class net {
  constructor(gameObject, height = 600, x = 0, y = 0) {
    this.self = gameObject.add.graphics();
    this.x = x;
    this.y = y;
    this.size = 1;
    this.height = height;
    this.scale = 1;
    this.rects = [];

    const line = 8;
    const space = 6;
    const segment = line + space;
    const available_space = height - line;
    const number_of_segments = available_space/segment;

    for (let i = 0; i < number_of_segments; i++) {
      this.rects.push(
        {
          x: this.x,
          y: i * segment,
          width: 2,
          height: line
        }
      );
    }
    this.rects.push({
      x: this.x,
      y: this.height - line,
      width: 2,
      height: line
    });
  }

  draw() {
    const rectangles = this.rects;
    this.self.fillStyle(0xffffff, 1);

    for (let i = 0; i < rectangles.length; i ++) {
      this.self.fillRect(
        rectangles[i].x,
        rectangles[i].y,
        rectangles[i].width,
        rectangles[i].height
      );
    };
  }
}