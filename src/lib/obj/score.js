import { numbers } from './font/numbers';

export class score {
  constructor(gameObject, x = 0, y = 0) {
    this.numbers = [];

    for (let i = 0; i < 10; i++) {
      this.numbers.push(new numbers(gameObject, x, y));
      this.numbers[i].draw(i);
    }
    this.setScore(0);
    this.currentScore = 0;
    this.maxScore = 10;
  }

  setMaxScore(n) {
    this.maxScore = n;
  }

  setScore(n) {
    if (n >= this.maxScore) {
      console.log('max score reached');
      return true;
    }

    this.currentScore = n;
    for (let i = 0; i < 10; i++) {
      this.numbers[i].setInVisible();
    }
    this.numbers[this.currentScore].setVisible();
  }

  incrementScore(n) {
    const gameStatus = this.setScore(this.currentScore + n);
    if (gameStatus === true) {
      return true;
    }
  }
}