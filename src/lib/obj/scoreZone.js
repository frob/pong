export class scoreZone {
  constructor(gameObject, zoneId, x = 0, y = 0, width, height) {
    this.self = gameObject.add.graphics();
    this.gameObject = gameObject;
    this.x = x;
    this.y = y;
    this.size = 1;
    this.scale = 1;
    this.width = width;
    this.height = height;

    this.self.fillStyle(0xff0000, 1);
    this.self.fillRect(this.x, this.y, this.width, this.height);
    this.self.generateTexture(zoneId, this.width, this.height);
    this.self.setVisible(true);
    this.physicsObject = this.gameObject.physics.add.image(this.x, this.y, zoneId);
  }
}
