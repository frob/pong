import { Scene } from 'phaser';
import { score } from '../lib/obj/score';
import { net } from '../lib/obj/net';
import { ball } from '../lib/obj/ball';
import { scoreZone } from '../lib/obj/scoreZone';
import { ScoreKeeper } from '../lib/obj/ScoreKeeper';
const RandomCrap = require('randomcrap');

class PongScene extends Scene {
  constructor() {
    super({
      key: "PongScene",
    });

    this.rc = new RandomCrap();
    // this.rc.setBias(0);
    this.ball = null;
  }

  preload () {
    this.leftScore = new score(this, 200, 30);
    this.rightScore = new score(this, 600, 30);
    this.net = new net(this, 600, 399, 0);
    this.net.draw();
    this.ball = new ball(this, 0, 0);

    this.score_width = 24 * 2.11;
    const score_width = this.score_width;
    this.scoreZoneLeft = new scoreZone(
      this, 
      'scoreZoneLeft', 
      0, 
      0, 
      score_width, 
      this.cameras.main.displayHeight
    );
    this.scoreZoneRight = new scoreZone(
      this, 
      'scoreZoneRight', 
      this.cameras.main.displayWidth - score_width, 
      0, 
      score_width, 
      this.cameras.main.displayHeight
    );
    this.scoreKeeper = new ScoreKeeper(
      this,
      this.ball,
      [
        this.scoreZoneLeft,
        this.scoreZoneRight
      ],
      [
        this.leftScore,
        this.rightScore
      ],
      {
        'left': 0,
        'right': 1
      }
    );
  }
  
  create () {
    this.ball.serve(
      { 
        x: this.rc.simpleRandomInt(200, 300), 
        y: this.rc.simpleRandomInt(-130, 130)
      },
      [
        this.scoreZoneRight.physicsObject,
        this.scoreZoneLeft.physicsObject,
        this.scoreZoneRight.physicsObject,
        this.scoreZoneLeft.physicsObject
      ]
    );    
  }

  update() {
    if (this.ball.ballP.x + this.ball.ballWidth > this.scoreZoneRight.x) {
      this.ball.reserve("hit: Right");
      this.scoreKeeper.triggerScore('left');
    }

    if (this.ball.ballP.x < this.scoreZoneLeft.x + this.scoreZoneLeft.width) {
      this.ball.reserve("hit: Left");
      this.scoreKeeper.triggerScore('right');
    }
  }
}

export default PongScene;
